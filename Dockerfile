# Use the official OpenJDK image with Java 1.8
FROM maven:3-jdk-8-alpine

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy the project files into the container
COPY . /usr/src/app

# Build the application
RUN mvn package

# Set environment variables
ENV PORT 8080

# Expose the port
EXPOSE $PORT

# Define the command to run the application
CMD ["sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run"]
